/*
 * Compilación: $ make
 * Ejecución: $ ./programa
 */

// se incluyen las librerías
#include <list>
#include <iostream>
#include <stdlib.h>
using namespace std;

// se incluyen las clases
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

// Funcion que pide ingresar los datos de la proteina al usuario
list<Proteina> ingresar_datos(list<Proteina> proteinas){
	//variables
	Proteina prot = Proteina();
	string nombre;
	
	cout << "Ingresar nombre de proteina: ";
	cin >> nombre;
	prot.set_nombre(nombre);
	
	list<Cadena> cadena;
	prot.add_cadena(cadena);
  
	proteinas.push_back(prot);
	return proteinas;
}

// función imprime proteinas existentes
void imprimir(list<Proteina> proteinas){
	cout<<">> Proteínas <<" << endl;
	for (Proteina prot: proteinas){
		cout<<"***********************************" << endl;
		cout << "Nombre proteina: " << prot.get_nombre() << endl;
		for(Cadena cadenas: prot.get_cadenas()){
			cout << "Cadena de la proteina: " << cadenas.get_letra() << endl;
			for(Aminoacido aminoacidos: cadenas.get_aminoacidos()){
				cout << "> Cantidad de aminoacidos: " << aminoacidos.get_numero() << endl;
				cout << "> Nombre de aminoacido de cadena: " << aminoacidos.get_nombre() << endl;
				cout << "> Atomos " << endl;
				for(Atomo atomo: aminoacidos.get_atomos()){
					Coordenada coo = atomo.get_coordenada();
					cout << "> Atomo: " << atomo.get_numero() << ", Nombre: " << atomo.get_nombre() << endl;
					cout << "> Coordenadas " << endl;
					cout << "x: " << coo.get_x() << " | ";
					cout << "y: " << coo.get_y() << " | ";
					cout << "z: " << coo.get_z() << endl;
					
				}
			}
		}
    }
}


// Función de menú
void menu(list<Proteina> proteinas){
	int op;
	cout <<"      <<Base de proteínas>>"<<endl;
	cout <<" **************************************\n Escoja una opción:"<<endl;
	cout <<" 1. Ingresar proteina"<<endl;
	cout <<" 2. Ver proteinas"<<endl;
	cout <<" 3. Salir"<<endl;
	cout <<">> ";
	cin >> op;

	if (op == 1){
		proteinas = ingresar_datos(proteinas);
		menu(proteinas);
	}
	else if (op == 2){
		imprimir(proteinas);
		menu(proteinas);
	}
	else if (op == 3){
		cout << "Hasta la próxima" << endl;
		exit(0);
	}
	else{
		cout <<"Ingrese opción válida" <<endl;
		menu(proteinas);
	}
}


//Función Principal
int main(){
	list<Proteina> proteinas;
	menu(proteinas);

	return 0;
}
