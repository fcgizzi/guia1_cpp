//librerias
#include <list>
#include <iostream>
using namespace std;

//clases
#include "Aminoacido.h"
#include "Atomo.h"
#include "Coordenada.h"

//constructores
Aminoacido::Aminoacido(){
	string nombre = "\0";
	int numero = 0;
	list<Atomo> atomos;
}
Aminoacido::Aminoacido(string nombre, int numero, list<Atomo> atomos){
	this->nombre = nombre;
	this->numero = numero;
	this->atomos = atomos;
}

// metodos
// cada set con su get

//nombre
void Aminoacido::set_nombre(string nombre){
	this->nombre = nombre;
}
string Aminoacido::get_nombre(){
	return this->nombre;
}

//numero de aminoacido
void Aminoacido::set_numero(int numero){
	this->numero = numero;
}
int Aminoacido::get_numero(){
	return this->numero;
}

// agrega atomos a aminoacido
void Aminoacido::add_atomos(list<Atomo> atomos){
	int count; // cantidad de átomos
	cout << "Ingrese cantidad de átomos del aminoácido " << this->nombre <<": ";
	cin >> count;
	for(int i = 1; i < count + 1; i++){
		string atomo; // nombre del átomo
		Atomo atoms = Atomo(); // átomo
		cout << "Atomo número " << i << ":";
		cin >> atomo;
		atoms.set_nombre(atomo);
		atoms.set_numero(i);

		// llama a clasecordenadas para agregar
		Coordenada coordenada;
		atoms.add_coordenadas(coordenada);

		// se agrega el atomo a lista
		atomos.push_back(atoms);
	}
	this->atomos = atomos;
}

// retorna la lista de atomos
list<Atomo> Aminoacido::get_atomos(){
	return this->atomos;
}
