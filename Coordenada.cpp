//librerias
#include <list>
#include <iostream>
using namespace std;

//clase
#include "Coordenada.h"

// constructores
Coordenada::Coordenada(){
	float x = 0;
	float y = 0;
	float z = 0;
}
Coordenada::Coordenada(float x, float y, float z){
	this->x = x;
	this->y = y;
	this->z = z;
}

// metodos
// cada set con su get 

// set and get de x
void Coordenada::set_x(float x){
	this->x = x;
}
float Coordenada::get_x(){
	return this->x;
}
// set and get de y
void Coordenada::set_y(float y){
	this->y = y;
}
float Coordenada::get_y(){
	return this->y;
}

// set and get de z
void Coordenada::set_z(float z){
	this->z = z;
}
float Coordenada::get_z(){
	return this->z;
}
