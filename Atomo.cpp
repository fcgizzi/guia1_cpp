//librerias
#include <list>
#include <iostream>
using namespace std;

//clases
#include "Atomo.h"
#include "Coordenada.h"

// constructores
Atomo::Atomo(){
	string nombre = "\0";
	int numero = 0;
	Coordenada coordenada;
}
Atomo::Atomo(string nombre, int numero, Coordenada coordenada){
	this->nombre = nombre;
	this->numero = numero;
	this->coordenada = coordenada;
}

// metodos
// cada set con su get

// nombre ed atomo
void Atomo::set_nombre(string nombre){
	this->nombre = nombre;
}
string Atomo::get_nombre(){
	return this->nombre;
}

// numero de atomo
void Atomo::set_numero(int numero){
	this->numero = numero;
}
int Atomo::get_numero(){
	return this->numero;
}

// corrdenadas del atomo
void Atomo::add_coordenadas(Coordenada coordenada){
	float x, y, z;
	cout << "Ingresar: " << endl;
	cout << "Coordenada x: ";
	cin >> x;
	cout << "Coordenada y: ";
	cin >> y;
	cout << "Coordenada z: ";
	cin >> z;
	coordenada.set_x(x);
	coordenada.set_y(y);
	coordenada.set_z(z);
	this->coordenada = coordenada;
}

// se envia cada cordenada con mismo objeto
Coordenada Atomo::get_coordenada(){
	return this->coordenada;
}
