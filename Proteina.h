#include <list>
#include <iostream>
using namespace std;

#include "Cadena.h"

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina {
	private:
		string nombre = "\0";
		list<Cadena> cadena;
	public:
		Proteina();
		Proteina (string nombre, list<Cadena> cadena);
		string get_nombre();
		list<Cadena> get_cadenas();
		void set_nombre(string nombre);
		void add_cadena(list<Cadena> cadena);
};
#endif
