prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = programa.cpp Proteina.cpp Cadena.cpp Aminoacidos.cpp Atomo.cpp Coordenada.cpp
OBJ = programa.o Proteina.o Cadena.o Aminoacido.o Atomo.o Coordenada.o
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
