#include <list>
#include <iostream>
using namespace std;

#include "Cadena.h"
#include "Aminoacido.h"
#include "Atomo.h"

//constructores
Cadena::Cadena(){
	string letra = "\0";
	list<Aminoacido> aminoacidos;
}
Cadena::Cadena(string letra, list<Aminoacido> aminoacidos){
	this->letra = letra;
	this->aminoacidos = aminoacidos;
}

// metodos
// cada set con su get

//letra de cadena
void Cadena::set_letra(string letra){
	this->letra = letra;
}
string Cadena::get_letra(){
	return this->letra;
}

// aminoacidos que se ingreasarán en cadena
void Cadena::add_aminoacidos(list<Aminoacido> aminoacidos){
	int cantidad;
	cout << "Ingrese cantidad de aminoacidos de la cadena: " << this->letra << ": ";
	cin >> cantidad;
	
	for(int i=1; i<cantidad + 1; i++){
		Aminoacido amino = Aminoacido();
		string nombre;
		cout << "Ingrese nombre del aminoacido " << i << ": ";
		cin >> nombre;
		amino.set_nombre(nombre);
		amino.set_numero(i);
    
		list<Atomo> atomos;
		aminoacidos.push_back(amino);
	}
	this->aminoacidos = aminoacidos;
}

// aminoacidos pasan a una lista
list<Aminoacido> Cadena::get_aminoacidos(){
	return this->aminoacidos;
}
