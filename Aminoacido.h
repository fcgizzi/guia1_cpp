//librerias
#include <list>
#include <iostream>
using namespace std;

//clases
#include "Atomo.h"

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
	private:
		string nombre = "\0";
		int numero = 0;
		list<Atomo> atomos;
	public:
		Aminoacido();
		Aminoacido (string nombre, int numero, list<Atomo> atomos);
		void set_nombre(string nombre);
		void set_numero(int numero);
		void add_atomos(list<Atomo> atomos);
		string get_nombre();
		int get_numero();
		list<Atomo> get_atomos();
};
#endif
