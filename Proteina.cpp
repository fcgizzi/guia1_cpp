//librerias
#include <list>
#include <iostream>
using namespace std;

//clases
#include "Proteina.h"
#include "Cadena.h"
#include "Aminoacido.h"

// Se definen constructores
Proteina::Proteina() {
    string nombre = "\0";
    list<Cadena> cadena; 
}
Proteina::Proteina(string nombre, list<Cadena> cadena){
    this->nombre = nombre;
    this->cadena = cadena;
}

// Métodos
// cada set con su get

// nombre de proteina
void Proteina::set_nombre(string nombre){
    this->nombre = nombre;
}
string Proteina::get_nombre(){
    return this->nombre;
}

// se definen las cadenas
void Proteina::add_cadena(list<Cadena> cadena){
	int count;
	int i;
	cout << "Cantidad de cadenas que componen su proteina: ";
	cin >> count;
	for(i = 0; i < count; i++){
		string letra;
		Cadena c = Cadena();
		cout << " Ingrese identificador de cadena (" << i + 1 <<"): ";
		cin >> letra;
		c.set_letra(letra);
		
		list<Aminoacido> aminoacidos;
		c.add_aminoacidos(aminoacidos);
		cadena.push_back(c);
	}
	this->cadena = cadena;
}

// cadenas añadidas
list<Cadena> Proteina::get_cadenas(){
	return this->cadena;
}
