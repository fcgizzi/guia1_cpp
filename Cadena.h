#include <list>
#include <iostream>
using namespace std;

#include "Aminoacido.h"

#ifndef CADENA_H
#define CADENA_H

class Cadena {
	private:
		string letra = "\0";
		list<Aminoacido> aminoacidos;
	public:
		Cadena();
		Cadena (string letra, list<Aminoacido> aminoacidos);
		void set_letra(string letra);
		string get_letra();
		list<Aminoacido> get_aminoacidos();
		void add_aminoacidos(list<Aminoacido> aminoacidos);
};
#endif
